# Knave
This is a system for Foundry Virtual Tabletop (https://foundryvtt.com/) that emulates Ben Milton's fabulous classless old-school RPG Knave.

It features an automatic character generator, an assortment of well known AD&D Monsters and a full set of icons for the games tools and gear.

### Installation
To install the Knave system, copy and paste this link [https://gitlab.com/nimzodisaster/knaves-of-the-dark-foundryvtt/-/raw/master/system.json]  into the Manifest URL section of the Install System dialog in the Foundry client.

## Automatic Character Generator
Quickly and easily generate new characters on the fly at the click of a button.

![Character Generation](https://gitlab.com/nimzodisaster/knaves-of-the-dark-foundryvtt/-/raw/master/img/previews/chargen.gif "Character Generation")

## Monster Cards
Easily manage and create monsters with simple card based design.

![Monster Card](https://gitlab.com/nimzodisaster/knaves-of-the-dark-foundryvtt/-/raw/master/img/previews/gnomecard.png "Monster Card")
![Monster Attacks](https://gitlab.com/nimzodisaster/knaves-of-the-dark-foundryvtt/-/raw/master/img/previews/attacks.png "Monster Attacks displayed in chat")

## In Built Bestiary
A selection of AD&D's most iconic monsters built into the system itself.

![Bestiary](https://gitlab.com/nimzodisaster/knaves-of-the-dark-foundryvtt/-/raw/master/img/previews/bestiary.png "Bestiary")

## Thanks
Special thanks to Anathema, nikolaj-a, asocalips and solarbear on the Foundry VTT Discord for helping me through the creation of this system. A HUGE thanks to Rabid Baboon for giving me the inpsiration to start!
